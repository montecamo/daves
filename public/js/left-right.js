document.addEventListener('DOMContentLoaded', () => {
    let leftBtn = document.getElementById('left-btn');
    let rightBtn = document.getElementById('right-btn');

    let tileLeft = document.getElementById('tile-left');
    let tileRight = document.getElementById('tile-right');
    
    function goToGarage() {
        window.location = 'garage'; 
    }

    function goToRestaurant() {
        window.location = 'restaurant' 
    }

    leftBtn.addEventListener('click', () => {
        tileRight.classList.add('right-move'); 
        setTimeout(goToGarage, 500);
    });

    rightBtn.addEventListener('click', () => {
        tileLeft.classList.add('left-move'); 
        setTimeout(goToRestaurant, 500);
    });
});
