FROM node:latest as build

WORKDIR /app

COPY . .

RUN npm i

EXPOSE 80

ENTRYPOINT ["node", "Server.js"]
