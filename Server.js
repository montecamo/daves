const express       = require('express');
const app           = express();
const path          = require('path');
const cookieSession = require('cookie-session');
//const favicon       = require('serve-favicon');
//const mysql         = require('mysql');


/*let connection = mysql.createConnection({
    host                : 'localhost',
    user                : 'root',
    password            : '1',
    database            : 'daves'
 //   multipleStatements  : true
});

connection.connect((err) => {
        if (err) {
            console.log("Something went wrong ", err);
        }

        console.log("Successfully connected");
});*/

app.set('views', './views');
app.set('view engine', 'jade');

app.use(express.static(__dirname + '/public'));
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(express.json());
app.use(express.urlencoded({extended: true}));

/*app.use(cookieSession({
  name: 'session',
  keys: ['key'],
  maxAge: 24 * 60 * 60 * 1000 // 24 hours
}));*/

app.get('/', (req, res) => {
    res.render('home', {title: 'Home', type: 'home', images: ['images/gallery-img.png', 'images/gallery-img.png', 'images/gallery-img.png']});
});

app.get('/garage', (req, res) => {

    res.render('garage', {
                         title: 'Garage',
                         type: 'garage',
                         images: ['images/moto-3.png', 'images/moto-3.png', 'images/moto-3.png'],
                         //shopImages: ['images/shop-item.png', 'images/shop-item.png', 'images/shop-item.png',
                         //             'images/shop-item.png', 'images/shop-item.png', 'images/shop-item.png'],

                         workers: [{ src: 'images/worker.png', position: 'Position' },
                                   { src: 'images/worker.png', position: 'Position' },
                                   { src: 'images/worker.png', position: 'Position' },
                                   { src: 'images/worker.png', position: 'Position' }],

                         items: [{ name: 'Item', price: '365' },  { name: 'Item', price: '365' },
                                 { name: 'Item', price: '365' },  { name: 'Item', price: '365' },
                                 { name: 'Item', price: '365' },  { name: 'Item', price: '365' },
                                 { name: 'Item', price: '365' },  { name: 'Item', price: '365' },
                                 { name: 'Item', price: '365' },  { name: 'Item', price: '365' }],

                         reviews: [{ name: 'Jasmin', text: '  Lorem ipsum dolor sit amet, ' +
                                                           'consectetur adipiscing elit, ' +
                                                           'sed do eiusmod tempor incididunt ' +
                                                           'ut labore et dolore magna aliqua. ' +
                                                           'Ut enim ad minim veniam, quis ' +
                                                           'nostrud exercitation ullamco laboris ' +
                                                           'nisi ut aliquip ex ea commodo', stars: '5' },

                                   { name: 'Jasmin', text: '  Lorem ipsum dolor sit amet, ' +
                                                           'consectetur adipiscing elit, ' +
                                                           'sed do eiusmod tempor incididunt ' +
                                                           'ut labore et dolore magna aliqua. ' +
                                                           'Ut enim ad minim veniam, quis ' +
                                                           'nostrud exercitation ullamco laboris ' +
                                                           'nisi ut aliquip ex ea commodo', stars: '3' }]
                         });
});

app.get('/bar', (req, res) => {
    res.render('bar', { title: 'Bar',
                        type: 'bar',
                        menu: [ { name: 'item', src: 'images/shop-burger.png'},
                                { name: 'item', src: 'images/shop-burger.png'},
                                { name: 'item', src: 'images/shop-burger.png'},
                                { name: 'item', src: 'images/shop-burger.png'},
                                { name: 'item', src: 'images/shop-burger.png'},
                                { name: 'item', src: 'images/shop-burger.png'} ],

                        cooks: [ { name: ['N', 'A', 'M', 'E'], position: 'position', src: 'images/cook.png'},
                                 { name: ['N', 'A', 'M', 'E'], position: 'position', src: 'images/cook.png'} ],

                        reviews: [ { name: ['N', 'A', 'M', 'E'], text: '  Lorem ipsum dolor sit amet, ' +
                                                           'consectetur adipiscing elit, ' +
                                                           'sed do eiusmod tempor incididunt ' +
                                                           'ut labore et dolore magna aliqua. ' +
                                                           'Ut enim', stars: '5' },

                                   { name: ['N', 'A', 'M', 'E'], text: '  Lorem ipsum dolor sit amet, ' +
                                                           'consectetur adipiscing elit, ' +
                                                           'sed do eiusmod tempor incididunt ' +
                                                           'ut labore et dolore magna aliqua. ' +
                                                           'Ut enim', stars: '3' }]
                        });
});

/*app.get('/workers', (req, res) => {
    connection.query("SELECT * FROM workers", (err, result) => {
        if (err) {
            console.log(err);
            res.rediirect('/');
        }
        console.log(result);
        res.redirect('/');
    });
});*/

app.listen(80);

console.log('Magic on port 80');
